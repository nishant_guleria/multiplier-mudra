<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'multiplier-mudra' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T+gaz_[/)>F:fp`+]0xQ>+?PxX<o6rY7c.k!/7Mrn5C.k|gd[!IVMh#l|nY-EcX<' );
define( 'SECURE_AUTH_KEY',  'jYEqvCANtd$7=/dc7K:QF7JPx.anpGGZ|T=J*9W1=R~++<]LnP2uzq.Ky^GD$>`I' );
define( 'LOGGED_IN_KEY',    '>xiN.&JDuQ@l@tvI!A7OG=5PN,mHFLyh,5+C7yoP3~G@$#/~`6kInJWXn-@i.3O=' );
define( 'NONCE_KEY',        'NRy;TBF%S75MtM!cCqczcq!PK:vnr]_%J>Xo&>fCBA0|t%nzDT~zMBS6.]VV5z6U' );
define( 'AUTH_SALT',        '=U!Z);T-kGRX?j}OUfRMrpv9^/700p{_t*E-t?g@hO`FMxZ&#u#se#.}-5^tvm>X' );
define( 'SECURE_AUTH_SALT', ') @XISJ6?6Z)r=JylEDR^2)dZ+NpU?YAo-&GUc(X_J8tzfiQDiu/Z(0RQ&5zb#lB' );
define( 'LOGGED_IN_SALT',   '2[bvjT;<:4tFY$/Lvkkg^HD~p?/pkA{d+IKKu_giK=2}[U@gR+TcS1`;I`#4k128' );
define( 'NONCE_SALT',       '=pKBn%^mgqkz%I^xTU87LnQB$SK+wr`7%xdqe%SQ.sEQJ!vm)Knhm#eMc E_qUGh' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
